﻿#pragma once
#include "cannyDetect.h"
#include "square.h"

Squares::Squares() {
}
//fill the gap between two lines to connect lines that are supposed to be one line in vectors
void Squares::connectSimilarLines(std::vector<myStructs::LINE> &_lines) {

	//get the mean angle of each line
	std::vector<float> means;
	for (int i = 0; i < _lines.size(); i++) {
		double meanOfThisLine = 0;
		double sumOfAngles = 0;
		for (int j = 0; j < _lines[i].pixels.size(); j++) {
			sumOfAngles += _lines[i].pixels[j].angle;
		}
		meanOfThisLine = sumOfAngles / _lines[i].pixels.size();
		means.push_back(meanOfThisLine);
	}	

	//just fill 'isChecked' vector with false
	for (int c = 0; c < _lines.size(); c++) {
		bool thisLineIsChecked = false;
		_isChecked.push_back(thisLineIsChecked);
		bool thisLineIsConnected = false;
		_isConnected.push_back(thisLineIsConnected);
	}

	//make a new vector to store new lines, and also to avoid confusing by changing the original vector _lines
	for (int i = 0; i < _lines.size(); i++) {
		if (_isConnected[i] == true) //if the element is already found connected to an other line, skip
			continue;
		//initilise an empty line
		oneLine = {};

		for (int b = 0; b < _lines[i].pixels.size(); b++) {
			//by default, push to oneLine to make copy
			oneLine.pixels.push_back(_lines[i].pixels[b]);
			oneLine.length++;
		}

		for (int j = 0; j < _lines.size(); j++) {
			if (_isChecked[j] == true) //if the element is already checked, skip
				continue;
				//if the elements are not the same and the two lines are presumably perpendicular
			if (i != j && abs(means[i] - means[j]) < angleGap && _lines[i].pixels.back().y != _lines[i].pixels[0].y && _lines[i].pixels.back().x != _lines[i].pixels[0].x) {
				//get the actual angle of the line by calculation
				double lineAngle = static_cast<double>(_lines[i].pixels.back().y - _lines[i].pixels[0].y) / (double)(_lines[i].pixels.back().x - _lines[i].pixels[0].x);
				//just pick two random pixels in two lines b = y - ax, b1 and b2 are y axis value when x = 0
				double b1 = _lines[j].pixels[0].y - lineAngle * _lines[j].pixels.back().x;
				double b2 = _lines[i].pixels[0].y - lineAngle * _lines[i].pixels.back().x;

				//if the Y perpendicular is small enough, join two lines nd see them as one line, put pixels into a vector _oneLine in order to have two lines in one new line 	
				if (abs(b2 - b1) < yPerpendicularGap) {
					//mark j as a part of the line j to prevent from being checked again
					_isConnected[j] = true;
					for (int a = 0; a < _lines[j].pixels.size(); a++) {
						oneLine.pixels.push_back(_lines[j].pixels[a]);
						oneLine.length++;
					}

				}				
			}
		}
		_newLines.push_back(oneLine);
		//once j goes through all elements, mark i as checked
		_isChecked[i] = true;
	}
}

//set the length of lines we want to keep, avoid magic number
void Squares::getOuterSquare() {
	for (int i = 0; i < _newLines.size(); i++) {
		if (_newLines[i].length > outerSquareLength)
			_bigSquare.push_back(_newLines[i]);
	}
}

void Squares::linearLeastSquaresFitting() {
	/*The steps of linear least squares fitting is as follows
	1. Calculate the means of the x values and y values (xMean, yMean)
	2. Calculate (x - xMean)(y - yMean) and sqrt(x - xMean) for each
	3. Calculate the slope: sum of (x - xMean)(y - yMean) / sum of sqrt(x - xMean)
	4. Calculate the y intercept: yMean - slope * xMean
	5. Get y = mx + b
	*/

	for (int i = 0; i < _bigSquare.size(); i++) {
		//Calculate the means of the x values and y values (xMean, yMean)
		int totalX = 0;
		int totalY = 0;
		int numberOfElements = 0;

		for (int j = 0; j < _bigSquare[i].pixels.size(); j++) {
			totalX += _bigSquare[i].pixels[j].x;
			totalY += _bigSquare[i].pixels[j].y;
			numberOfElements++;
		}
		double xMean = totalX / numberOfElements;
		double yMean = totalY / numberOfElements;

		//Calculate the slope: sum of (x - xMean)(y - yMean) / sum of sqrt(x - xMean)
		double sumOfThis = 0;
		double sumOfSqrt = 0;

		for (int j = 0; j < _bigSquare[i].pixels.size(); j++) {
			sumOfThis += (_bigSquare[i].pixels[j].x - xMean) * (_bigSquare[i].pixels[j].y - yMean);
			sumOfSqrt += (_bigSquare[i].pixels[j].x - xMean) * (_bigSquare[i].pixels[j].x - xMean);
		}
		_bigSquare[i].slope = sumOfThis / sumOfSqrt;

		//Calculate the y intercept: yMean - slope * xMean
		_bigSquare[i].yIntercept = yMean - _bigSquare[i].slope * xMean;
	}
}

void Squares::fitLeastSquareLines(cv::Mat &inputImage) {
	myStructs::LINE squareFittedLine;
	myStructs::PIXEL pixel;

	for (int i = 0; i < _bigSquare.size(); i++) {
	
		squareFittedLine = {}; //initialise the struct
		for (int x = 0; x <= inputImage.cols; x++) {
			//for (int y = 0; x <= inputImage.rows; y++) {
				pixel.x = x;
				pixel.y = _bigSquare[i].slope * x + _bigSquare[i].yIntercept;
				pixel.angle = _bigSquare[i].slope;
				squareFittedLine.pixels.push_back(pixel);
				squareFittedLine.length++;
		}
		_leastSquaredSquare.push_back(squareFittedLine);
		_leastSquaredSquare[i].slope = _bigSquare[i].slope;
		_leastSquaredSquare[i].yIntercept = _bigSquare[i].yIntercept;
	}
}

void Squares::findCorners(cv::Mat &inputImage) {
	myStructs::PIXEL pixel;

	_isChecked = {}; //initialise and fill the vector with false
	for (int x = 0; x < _leastSquaredSquare.size(); x++) {
		bool thisPixelIsChecked = false;
		_isChecked.push_back(thisPixelIsChecked);
	}

	//find the point where two lines cross and store in corners vector
	for (int i = 0; i < _leastSquaredSquare.size() - 1; i++) {
		for (int j = 1; j < _leastSquaredSquare.size(); j++) {
			if (i != j && _isChecked[j] == false) {
				// y = mx + b
				pixel.x = (_leastSquaredSquare[j].yIntercept - _leastSquaredSquare[i].yIntercept) / (_leastSquaredSquare[i].slope - _leastSquaredSquare[j].slope);
				pixel.y = _leastSquaredSquare[i].slope * pixel.x + _leastSquaredSquare[i].yIntercept;
				if (0 <= pixel.x && pixel.x < inputImage.cols && 0 <= pixel.y && pixel.y < inputImage.rows) {
					_corners.push_back(pixel);
					//go through the cross point of two lines and once its determined as a corner, add as a member of line struct as an end point value of (x, y)
					_leastSquaredSquare[i].edges.push_back(pixel);
					_leastSquaredSquare[j].edges.push_back(pixel);
				}
			}
		}
		_isChecked[i] = true;
	}
}

void Squares::drawLeastSquareLines(cv::Mat &inputImage) {
	myStructs::PIXEL endPixel0;
	myStructs::PIXEL endPixel1;

	myStructs::LINE finalLine;

	for (int i = 0; i < _leastSquaredSquare.size(); i++) {
		endPixel0 = _leastSquaredSquare[i].edges[0];
		endPixel1 = _leastSquaredSquare[i].edges[1];

		float dx = (float)abs(endPixel1.x - endPixel0.x);
		float dy = (float)abs(endPixel1.y - endPixel0.y);

		float L = (float)sqrt(dx * dx + dy * dy);

		float sx = dx / L;
		float sy = dy / L;

		finalLine = {};

		if (0 < abs(_leastSquaredSquare[i].slope) && abs(_leastSquaredSquare[i].slope) < 1) {
			for (int j = 0; j < L; j++) {
				myStructs::PIXEL pixel;
				pixel.x = endPixel1.x + (int)(sx * j);
				pixel.y = endPixel1.y - (int)(sy * j);
				finalLine.pixels.push_back(pixel);
			}
		}

		else {
			for (int j = 0; j < L; j++) {
				myStructs::PIXEL pixel;
				pixel.x = endPixel0.x + (int)(sx * j);
				pixel.y = endPixel0.y + (int)(sy * j);
				finalLine.pixels.push_back(pixel);
			}
		}

		_finalLines.push_back(finalLine);
	}
}

//drawing lines with a different colour per line for visualisation
void Squares::drawColourfulLines(cv::Mat &outputImage) {
	std::vector<myStructs::LINE> thisVector;
	thisVector = _finalLines;
	for (int i = 0; i < thisVector.size(); i++) {
		uchar r = (255 * rand() / RAND_MAX);
		uchar g = (255 * rand() / RAND_MAX);
		uchar b = (255 * rand() / RAND_MAX);
		uchar * ptr = outputImage.data;

		for (int j = 0; j < thisVector[i].pixels.size(); j++) {
			//avoid out of bounds on y direction as x direction is already done in previous function
			if (thisVector[i].pixels[j].y < outputImage.rows) {
				ptr[3 * (thisVector[i].pixels[j].x + thisVector[i].pixels[j].y * outputImage.cols)] = r;
				ptr[3 * (thisVector[i].pixels[j].x + thisVector[i].pixels[j].y * outputImage.cols) + 1] = g;
				ptr[3 * (thisVector[i].pixels[j].x + thisVector[i].pixels[j].y * outputImage.cols) + 2] = b;
			}
		}
	}
}

void Squares::drawCorners(cv::Mat &inputImage) {
	//colour white on corners
	for (int i = 0; i < _corners.size(); i++) {
		inputImage.at<cv::Vec3b>(_corners[i].y, _corners[i].x) = cv::Vec3b(255, 255, 255);
	}
}

Squares::~Squares() {

}