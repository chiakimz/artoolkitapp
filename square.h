#pragma once
#include <iostream>
#include "camera.h"
#include "cannyDetect.h"
#include "common.h"


class Squares {
private:
	std::vector<bool> _isChecked;
	std::vector<bool> _isConnected;
	std::vector<myStructs::LINE> _newLines;
	std::vector<myStructs::LINE> _bigSquare;
	
	myStructs::LINE oneLine;

	std::vector<myStructs::PIXEL> _corners;

	std::vector<myStructs::LINE> _leastSquaredSquare;
	std::vector<myStructs::LINE> _finalLines;

public:
	Squares(); // Constructor function needs to be present in all your classes - and should initialise themselves...
	~Squares(); // destructor function 

	void connectSimilarLines(std::vector<myStructs::LINE> &_lines);
	int outerSquareLength;
	void getOuterSquare();
	void linearLeastSquaresFitting();
	void fitLeastSquareLines(cv::Mat &inputImage);
	void findCorners(cv::Mat &inputImage);
	void drawLeastSquareLines(cv::Mat &inputImage);
	void drawColourfulLines(cv::Mat &outputImage);
	void drawCorners(cv::Mat &inputImage);

	float angleGap;
	float yPerpendicularGap;
	
};
