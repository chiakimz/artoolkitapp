#pragma once
#include <iostream>
#include "camera.h"
#include "cannyDetect.h"
#include "common.h"
#include "square.h"
class poseEstimation
{
private:
	void getEstimatedPose();
public:
	poseEstimation();
	~poseEstimation();
};

