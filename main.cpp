#pragma once

#if !defined(TESTING)

#include <iostream>
#include "camera.h"
#include "cannyDetect.h"
#include "square.h"

int main() {
	//initialise classes and make an instance each
	Camera camera;
	CannyEdgeDetection canny;
	Squares sqr;

	//initialise image
	cv::Mat image;
	
	//initialise vector
	std::vector<std::vector<cv::Point> > squaresVector;
	std::vector<std::vector<double>> filter = canny.createGaussianFilter(3, 3, 1);

	//put all the numbers that need adjusting in one place here
	canny.lowerThreshold = 0.1;
	canny.upperThreshold = 0.25;
	canny.minimumLineLength = 60;
	canny.hysteresisAngleGap = 0.1; //difference in pixel angles
	sqr.angleGap = 0.1; //difference in pixel angles
	sqr.yPerpendicularGap = 130; //length between two points on y intercept
	sqr.outerSquareLength = 150;
	
	while (true) {
		//get video image from camera 
		//camera.getImage(image);
		image = cv::imread("marker1.png");
		imshow("raw image", image);
		cv::Mat gaussianBlurImage(image.rows, image.cols, image.type());
		canny.applyGaussFilter(image, gaussianBlurImage, filter);
		cv::Mat xDirectionImage = cv::Mat(gaussianBlurImage.size(), CV_32F);
		cv::Mat yDirectionImage = cv::Mat(gaussianBlurImage.size(), CV_32F);
		canny.gradientImageX(xDirectionImage, gaussianBlurImage);
		canny.gradientImageY(yDirectionImage, gaussianBlurImage);
		cv::Mat intensityImage = cv::Mat(gaussianBlurImage.size(), CV_32F);
		canny.intensityGradient(xDirectionImage, yDirectionImage, intensityImage);
		cv::Mat viewInt;
		viewInt = intensityImage / 1024;
		cv::Mat gradientImage = cv::Mat(gaussianBlurImage.size(), CV_32F);
		canny.gradientDirection(xDirectionImage, yDirectionImage, gradientImage);
		cv::Mat nonMaxSuppressedImage = cv::Mat::zeros(gradientImage.size(), CV_8UC1);
		canny.nonMaxSuppression(gradientImage, viewInt, nonMaxSuppressedImage);
		cv::Mat hysteresisImage = cv::Mat::zeros(nonMaxSuppressedImage.size(), CV_8UC1);

		std::vector<myStructs::LINE> semiFinalLines;

		canny.hysteresisStack(nonMaxSuppressedImage, gradientImage, hysteresisImage, canny.upperThreshold, canny.lowerThreshold, semiFinalLines);
		sqr.connectSimilarLines(semiFinalLines);
		sqr.getOuterSquare();
		
		sqr.linearLeastSquaresFitting();
		cv::Mat linesOnImage = cv::Mat::zeros(hysteresisImage.size(), CV_8UC3);
		sqr.fitLeastSquareLines(linesOnImage);
		sqr.findCorners(linesOnImage);
		sqr.drawLeastSquareLines(linesOnImage);
		sqr.drawColourfulLines(linesOnImage);
		sqr.drawCorners(linesOnImage);
       
		//show the image as a result
		imshow("output image", linesOnImage);
		cv::waitKey(0);
	}
	return 0;
}
#endif
