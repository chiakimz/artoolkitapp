#include "camera.h"
#include <iostream>

Camera::Camera() {
	// Open video capture device when you initialise camera class
	_cap.open(0); 

	// Check if video capture was opened properly
	if (!_cap.isOpened()) {
		std::cout << "Error opening video stream or file" << std::endl;
		exit(0); // quit application on error
	}
}

void Camera::getImage(cv::Mat &image) {
	// Capture next image from device and put into passed image reference
	_cap >> image;

	// check if captured image is ok - might be end of video
	if (image.empty()) {
		std::cout << "Error capturing image from video device" << std::endl;
		exit(0);
	}

	//quit the program when esc was pressed. Otherwise keep the videoCapture open
	if (cv::waitKey(1) == 27) {
		if (_cap.isOpened())
			_cap.release();
		else
			_cap.open(0);
	}
	//when showing the output image, and then go back to the top, meaning it keeps capturing images = video image
}

Camera::~Camera() {
	// close capture device on class destruction (app exit)
	_cap.release();
}

