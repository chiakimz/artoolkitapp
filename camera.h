#pragma once
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
class Camera {
private: // private variable that cannot be viewed or changed by other classes
	// all class variables should start with '_' and use camelcase
	cv::VideoCapture _cap;

public:
	Camera(); // Constructor function needs to be present in all your classes - and should initialise themselves...
	~Camera(); // destructor function 
	void getImage(cv::Mat &image);
};