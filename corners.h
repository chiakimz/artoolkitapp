#pragma once
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

class Corners {
private:
	int _blockSize;
	int _apertureSize;
	double _k;
	int _thresh;
	cv::Mat _grayImage;
	cv::Mat _harrisDetectedImage;
	cv::Mat _corners, _cornersScaled;

public:
	Corners(); //constructor
	~Corners(); //destructor
	void findCorners(cv::Mat &image);
};

