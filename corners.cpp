#include "corners.h"

Corners::Corners() {

}

void Corners::findCorners(cv::Mat &image) {
	_blockSize = 2; //neighbourhood size
	_apertureSize = 3; //aperture parameter for the sobel operator
	_k = 0.04; //harris detector free parameter
	_thresh = 50;

	//make the input image black and white
	_grayImage;
	cv::cvtColor(image, _grayImage, cv::COLOR_BGR2GRAY);

	_harrisDetectedImage = cv::Mat::zeros(image.size(), CV_32FC1);

	//apply cornerHarris to find corners
	cornerHarris(_grayImage, _harrisDetectedImage, _blockSize, _apertureSize, _k);
		
	_corners, _cornersScaled; //both are images
	cv::normalize(_harrisDetectedImage, _corners, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
	//Scales, calculates absolute values, and converts the result to 8-bit
	cv::convertScaleAbs(_corners, _cornersScaled); //input and output images
	
	//go through the image (= array)
	for (int i = 0; i < _corners.rows; i++) {
		for (int j = 0; j < _corners.cols; j++) {
			//if the pixel point value is bigger than the threshold, draw circle (= that's the corner)
			if ((int)_corners.at<float>(i, j) > _thresh) {
				cv::circle(_cornersScaled, cv::Point(j, i), 5, cv::Scalar(0), 2, 8, 0);
			}
		}
	}
}
Corners::~Corners() {

}
