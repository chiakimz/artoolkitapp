#pragma once
#include <vector>

namespace myStructs{
	struct PIXEL {
		int x;
		int y;
		float angle;
	};

	struct LINE {
		std::vector<PIXEL> pixels;
		int length;
		double slope;
		double yIntercept;
		std::vector<PIXEL> edges; //which will store edges when finding corners. should store two elements
	};
}