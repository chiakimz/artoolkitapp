#pragma once
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "camera.h"
#include <stack>
#include "common.h"

class CannyEdgeDetection {

private:

	std::stack<myStructs::PIXEL> _processStack;

	cv::Mat angles;
	cv::Mat mask;

	
public:
	CannyEdgeDetection(); //constructor
	~CannyEdgeDetection(); //destructor
	std::vector<std::vector<double>> createGaussianFilter(int row, int column, double sigma);
	cv::Mat	applyGaussFilter(cv::Mat &inputImage, cv::Mat &outputImage, std::vector<std::vector<double>> filter);
	void gradientImageX(cv::Mat &outputImage, cv::Mat &inputImage);
	void gradientImageY(cv::Mat &outputImage, cv::Mat &inputImage);
	void intensityGradient(cv::Mat &inputImageX, cv::Mat &inputImageY, cv::Mat &outputImage);
	void gradientDirection(cv::Mat &inputImageX, cv::Mat &inputImageY, cv::Mat &outputImage);
	void nonMaxSuppression(cv::Mat &gradientImage, cv::Mat &inputImage, cv::Mat &outputImage);
	void hysteresisStack(cv::Mat &nonMaxImage, cv::Mat &inputImageWithAngles, cv::Mat &outputImage, float &upperThreshold, float &lowerThreshold, std::vector<myStructs::LINE> &theseLines);
		
	float upperThreshold;
	float lowerThreshold;
	int _i;
	int _j;

	int minimumLineLength;

	float hysteresisAngleGap;
};

