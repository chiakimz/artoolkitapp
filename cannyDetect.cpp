#pragma once
#include "cannyDetect.h"

const double PI = 3.141;

CannyEdgeDetection::CannyEdgeDetection() {
}

std::vector<std::vector<double>> CannyEdgeDetection::createGaussianFilter(int row, int column, double sigma) {
	std::vector<std::vector<double>> gaussFilter;

	for (int i = 0; i < row; i++) {
		std::vector<double>col;
		for (int j = 0; j < column; j++) {
			col.push_back(-1);
		}
		gaussFilter.push_back(col);
	}

	float coordSum = 0; 
	double constant = 2.0 * sigma * sigma;

	float sum = 0.0;

	for (int x = -row / 2; x <= row / 2; x++) {
		for (int y = -column / 2; y <= column / 2; y++) {
			coordSum = (x * x + y * y);
			gaussFilter[x + row / 2][y + column / 2] = (exp(-(coordSum) / constant)) / (PI * constant);
			sum += gaussFilter[x + row / 2][y + column / 2];

		}
	}

	for (int i = 0; i < row; i++)
		for (int j = 0; j < column; j++)
			gaussFilter[i][j] /= sum;

	return gaussFilter;
}

cv::Mat CannyEdgeDetection::applyGaussFilter(cv::Mat &inputImage, cv::Mat &outputImage, std::vector<std::vector<double>> filter) {
	cvtColor(inputImage, inputImage, CV_RGB2GRAY);
	int size = (int)(filter.size() - 1) / 2;
	outputImage = cv::Mat(inputImage.rows - 2 * size, inputImage.cols - 2 * size, CV_8UC1);
	for (int i = size; i < inputImage.rows - size; i++) {
		for (int j = size; j < inputImage.cols - size; j++) {
			float sum = 0;

			for(int x = 0; x < filter.size(); x++)
				for (int y = 0; y < filter.size(); y++) {
					sum += filter[x][y] * (float)(inputImage.at<uchar>(i + x - size, j + y - size));
				}
			outputImage.at<uchar>(i - size, j - size) = sum;
		}
	}
	return outputImage;
}

//X Direction gradient image 
void CannyEdgeDetection::gradientImageX(cv::Mat &outputImage, cv::Mat &inputImage) {
	for (int i = 1; i < inputImage.rows - 1; i++) {
		for (int j = 1; j < inputImage.cols - 1; j++) {
			float total = ((float)inputImage.at<uchar>(i, j + 1) * 2 + (float)inputImage.at<uchar>(i - 1, j + 1) + (float)inputImage.at<uchar>(i + 1, j + 1)) - ((float)inputImage.at<uchar>(i, j - 1) * 2 + (float)inputImage.at<uchar>(i - 1, j - 1) + (float)inputImage.at<uchar>(i + 1, j - 1));
			outputImage.at<float>(i, j) = total;
		}
	}
}

//Y Direction gradient image
void CannyEdgeDetection::gradientImageY(cv::Mat &outputImage, cv::Mat &inputImage) {
	for (int i = 1; i < inputImage.rows - 1; i++) {
		for (int j = 1; j < inputImage.cols - 1; j++) {
			float total = ((float)inputImage.at<uchar>(i + 1, j) * 2 + (float)inputImage.at<uchar>(i + 1, j - 1) + (float)inputImage.at<uchar>(i + 1, j + 1)) - ((float)inputImage.at<uchar>(i - 1, j) * 2 + (float)inputImage.at<uchar>(i - 1, j - 1) + (float)inputImage.at<uchar>(i - 1, j + 1));
			outputImage.at<float>(i, j) = total;
		}
	}
}

void CannyEdgeDetection::intensityGradient(cv::Mat &inputImageX, cv::Mat &inputImageY, cv::Mat &outputImage){
	for (int i = 0; i < outputImage.rows; i++) {
		for (int j = 0; j < outputImage.cols; j++) {
			float g = std::sqrt(inputImageX.at<float>(i, j) * inputImageX.at<float>(i, j) + inputImageY.at<float>(i, j) * inputImageY.at<float>(i, j));
			outputImage.at<float>(i, j) = g;
		}
	}
}

//from both X and Y gradient images, get one image with magnitude
void CannyEdgeDetection::gradientDirection(cv::Mat &inputImageX, cv::Mat &inputImageY, cv::Mat &outputImage) {
	for (int i = 0; i < outputImage.rows; i++) {
		for (int j = 0; j < outputImage.cols; j++) {
			float theta = atan(inputImageY.at<float>(i, j)/inputImageX.at<float>(i, j));
			outputImage.at<float>(i, j) = theta;
		}
	}
}

void CannyEdgeDetection::nonMaxSuppression(cv::Mat &gradientImage, cv::Mat &inputImage, cv::Mat &outputImage) {
	//make each pixel in the image angle
	cv::Mat anglesOnImage = cv::Mat::zeros(gradientImage.rows, gradientImage.cols, CV_32FC1);
	for (int i = 1; i < gradientImage.rows - 1; i++) {
		for (int j = 1; j < gradientImage.cols - 1; j++) {
			anglesOnImage.at<float>(i, j) = (gradientImage.at<float>(i, j) * 180) / PI;
		}
	}

	inputImage.copyTo(outputImage); //output image is a copy of input image so that the changes in pixels are not reflected on the below loop
	
	for (int i = 1; i < anglesOnImage.rows - 1; i++) {
		for (int j = 1; j < anglesOnImage.cols - 1; j++) {
			float currentPixelsAngle = anglesOnImage.at<float>(i, j);
			float currentPixelIntensity = inputImage.at<float>(i, j);
		
			//angle roughly 0 degree direction
			if (-22.5 <= currentPixelsAngle && currentPixelsAngle < 22.5) {
				if (inputImage.at<float>(i, j - 1) > currentPixelIntensity || inputImage.at<float>(i, j + 1) > currentPixelIntensity)
						outputImage.at<float>(i, j) = 0;
			}
			//angle roughly 45 degrees direction or angle roughly -135 degrees direction
			else if ((22.5 <= currentPixelsAngle && currentPixelsAngle < 67.5) || (-157.5 <= currentPixelsAngle && currentPixelsAngle < -112.5)) {
				if (inputImage.at<float>(i - 1, j - 1) > currentPixelIntensity || inputImage.at<float>(i + 1, j + 1) > currentPixelIntensity)
					outputImage.at<float>(i, j) = 0;
			}
			//angle roughly 90 degrees direction or angle roughly -90 degrees direction
			else if ((67.5 <= currentPixelsAngle && currentPixelsAngle < 112.5) || (-112.5 <= currentPixelsAngle && currentPixelsAngle < -67.5)) {
				if (inputImage.at<float>(i - 1, j) > currentPixelIntensity || inputImage.at<float>(i + 1, j) > currentPixelIntensity)
					outputImage.at<float>(i, j) = 0;
			}
			//angle roughly 135 degrees direction or angle roughly -45 degrees direction
			else if ((112.5 <= currentPixelsAngle && currentPixelsAngle < 157.5) || (-67.5 <= currentPixelsAngle && currentPixelsAngle < -22.5)) {
				if (inputImage.at<float>(i + 1, j - 1) > currentPixelIntensity || inputImage.at<float>(i - 1, j + 1) > currentPixelIntensity)
					outputImage.at<float>(i, j) = 0;
			}
			//angle roughly 180 degrees direction
			else if ((157.5 <= currentPixelsAngle && currentPixelsAngle <= 180) || currentPixelsAngle < -157.5) {
				if (inputImage.at<float>(i - 1, j) > currentPixelIntensity || inputImage.at<float>(i + 1, j) > currentPixelIntensity)
					outputImage.at<float>(i, j) = 0;
			}
		}
	}
}

void CannyEdgeDetection::hysteresisStack(cv::Mat &nonMaxImage, cv::Mat &inputImageWithAngles, cv::Mat &outputImage, float &upperThreshold, float &lowerThreshold, std::vector<myStructs::LINE> &theseLines) {
	myStructs::PIXEL pixel;
	myStructs::LINE line;

	mask = cv::Mat::zeros(cv::Size(nonMaxImage.cols - 2, nonMaxImage.rows - 2), CV_16S);
	for (int i = 1; i < mask.rows; i++) {
		for (int j = 1; j < mask.cols; j++) {
			//mask.at<short>(i, j) == 0 means it hasn't been checked yet
			if (mask.at<short>(i, j) == 0 && nonMaxImage.at<float>(i, j) > upperThreshold) { //above upperthesh means it should be marked as an edge automatically
				mask.at<short>(i, j) = 1; //assign it to one to mark that it's an edgel
				pixel.x = j; //assign values to the instances of PIXEL struct
				pixel.y = i;
				pixel.angle = inputImageWithAngles.at<float>(i, j);
				_processStack.push(pixel); //put the edgel into the stack
				line.pixels.clear();
				line.length = 0;

				while (_processStack.size() > 0) { //as long as the stack is not empty
					pixel = _processStack.top();
					_processStack.pop(); //get the pixel on top of the stack out
					line.pixels.push_back(pixel);
					line.length++;

					//a and b; 3 x 3 pixels around the (pixel.x, pixel.y) to check the direction of 8 connected neighbouring pixels
					for (int a = -1; a <= 1; a++) { //a can be -1, 0, 1
						for (int b = -1; b <= 1; b++) { //b can be -1, 0, 1
							if (0 <= pixel.x + b && 0 <= pixel.y + a && pixel.x + b < mask.cols && pixel.y + a < mask.rows) {
								if (mask.at<short>(pixel.y + a, pixel.x + b) != 1) {
									if (abs(inputImageWithAngles.at<float>(pixel.y + a, pixel.x + b) - pixel.angle) < hysteresisAngleGap) { //make sure it's not out of bounce, and it's not marked as an edge, and it's a similar angle to the current pixel
										if (nonMaxImage.at<float>(pixel.y + a, pixel.x + b) > lowerThreshold) {
											mask.at<short>(pixel.y + a, pixel.x + b) = 1;
											myStructs::PIXEL pixel2;
											pixel2.x = pixel.x + b;
											pixel2.y = pixel.y + a;
											pixel2.angle = inputImageWithAngles.at<float>(pixel.y + a, pixel.x + b);
											_processStack.push(pixel2);
										}
									}
								}
							}
						}
					}
				}
				if (line.length > minimumLineLength) {
					theseLines.push_back(line);
				}
			}
		}
	}
	mask *= 255; //turned the pixels marked as 1 into 255
	mask.convertTo(outputImage, CV_8UC1); //make it visible by converting the image type
}

CannyEdgeDetection::~CannyEdgeDetection() {};
